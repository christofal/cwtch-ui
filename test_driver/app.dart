import 'package:flutter/widgets.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:cwtch/main.dart';

void main() {
  enableFlutterDriverExtension();
  runApp(Flwtch());
}